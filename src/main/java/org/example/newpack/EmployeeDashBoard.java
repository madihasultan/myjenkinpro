package org.example.newpack;
import java.util.Collection;
import org.example.newpack.Employee;
public interface EmployeeDashBoard {
    public Employee createEmployee(Employee employee);
    public Collection<Employee> getAllEmployees();

}
